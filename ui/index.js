module.exports = function mainUi(data) {
    return {
        root: {
            type: "container",
            children: [
                {
                    type: "text",
                    value: `${data.message} V1`
                },
                {
                    type: "button",
                    value: "Reset",
                    listeners: {
                        onClick: {
                            action: 'InitData'
                        }
                    }
                },
                {
                    type: "button",
                    value: "Button test",
                    listeners: {
                        onClick: {
                            action: 'Test'
                        }
                    }
                },
                {
                    type: "button",
                    value: "Disabled",
                    disabled: true
                },
                {
                    type: "checkbox",
                    value: data.checkboxValue,
                    label: data.checkboxLabel,
                    listeners: {
                        onChange: {
                            action: 'checkboxAction'
                        }
                    }
                },
                {
                    type: "checkbox",
                    value: true,
                    label: "Disabled",
                    disabled: true
                },
                {
                    type: "radio",
                    value: "machin",
                    label: `machin (${data.radioGroupValue})`,
                    groupValue: data.radioGroupValue,
                    listeners: {
                        onChange: {
                            action: 'radioAction'
                        }
                    }
                },
                {
                    type: "radio",
                    value: "truc",
                    label: `truc (${data.radioGroupValue})`,
                    groupValue: data.radioGroupValue,
                    listeners: {
                        onChange: {
                            action: 'radioAction'
                        }
                    }
                },
                {
                    type: "radio",
                    value: "foo",
                    groupValue: data.radioGroupValue,
                    label: "Disabled",
                    disabled: true,
                },
                {
                    type: "textfield",
                    value: "",
                    hintText: "hint",
                    description: "description",
                    label: "label",
                    obscure: false,
                    disabled: false,
                    inRow: false,
                    errorMessage: "error",
                    listeners: {
                        onChange: {
                            action: 'radioAction'
                        }
                    }
                },
                {
                    type: "textfield",
                    value: data.password,
                    hintText: "hint",
                    description: "description",
                    label: "label",
                    obscure: true,
                    disabled: false,
                    inRow: true,
                    errorMessage: data.passwordFieldError,
                    error: data.passwordFieldIsError,
                    listeners: {
                        onChange: {
                            action: 'passwordFieldAction'
                        }
                    }
                },
                {
                    type: "textfield",
                    value: "",
                    label: "Big",
                    hintText: "Big",
                    description: "Big",
                    width: 800.0,
                },
                {
                    type: "container",
                    direction: "row",
                    children: [
                        {
                            type: "text",
                            value: "row_el1"
                        },
                        {
                            type: "text",
                            value: "row_el2"
                        }
                    ]
                },
                {
                    type: "container",
                    direction: "col",
                    children: [
                        {
                            type: "text",
                            value: "col_el1"
                        },
                        {
                            type: "text",
                            value: "col_el2"
                        }
                    ]
                }
            ]
        }
    };
}