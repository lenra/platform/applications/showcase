
module.exports = function changeLabel(data, event) {
    data.checkboxValue = event.value;
    data.checkboxLabel = event.value ? "Coché" : "Décoché";
    return data;
}
