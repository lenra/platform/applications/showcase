module.exports = function initData() {
    return {
        message: "Bienvenue dans l'application Showcase",
        checkboxLabel: "Décoché",
        checkboxValue: false,
        radioGroupValue: "",
        passwordFieldIsError: false,
        passwordFieldError: "Default error",
        password: ""
    };
}

