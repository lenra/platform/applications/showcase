'use strict'

const initData = require('./InitData');
const changeNumber = require('./Test');
const changeLabel = require('./checkboxAction');

module.exports = async (action, data, props, event) => {
  switch (action) {
    case "InitData":
      return initData();
    case "Test":
      return changeNumber(data);
    case "checkboxAction":
      return changeLabel(data, event);
    case "radioAction":
      return changeRadio(data, event);
    case "passwordFieldAction":
      return passwordFieldAction(data, event);
    default:
      return {};
  }
}

function changeRadio(data, event) {
  data.radioGroupValue = event.value;
  return data;
}

function passwordFieldAction(data, event) {
  if(event.value.length < 4 || event.value.length > 32) {
    data.passwordFieldIsError = true;
    data.passwordFieldError = "Votre mot de passe doit contenir entre 4 et 32 caractères";
  } else {
    data.passwordFieldIsError = false;
  }

  data.password = event.value;
  
  return data;
}
